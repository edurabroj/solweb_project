<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>
    <jsp:body>
     	<h1>Inicio</h1>
		<h2>Bienvenido al Sistema de Inquilinos</h2>
		<h3>Usa el menú superior para navegar</h3>
    </jsp:body>
</t:layout>