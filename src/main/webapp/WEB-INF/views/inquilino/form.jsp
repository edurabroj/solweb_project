<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>
    <jsp:body>
     	<h2>Inquilino</h2>
		<form:form method="post" commandName="inquilino">			
			<div>
				<form:label path="nombre">Nombre</form:label>
				<form:input path="nombre" cssClass="form-control"/>
				<form:errors path="nombre"></form:errors>
			</div>			
			<div>
				<form:label path="aPaterno">Apellido Paterno</form:label>
				<form:input path="aPaterno" cssClass="form-control"/>
				<form:errors path="aPaterno"></form:errors>
			</div>		
			<div>
				<form:label path="aMaterno">Apellido Materno</form:label>
				<form:input path="aMaterno" cssClass="form-control"/>
				<form:errors path="aMaterno"></form:errors>
			</div>		
			<div>
				<form:label path="dni">DNI</form:label>
				<form:input path="dni" cssClass="form-control"/>
				<form:errors path="dni"></form:errors>
			</div> <br>	
			<div>
				<input type="submit" value="Guardar" class="btn btn-success"/>
			</div>		
		</form:form>
    </jsp:body>
</t:layout>