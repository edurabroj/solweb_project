<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>	
    <jsp:body>   
     	<h2>Inquilinos</h2> 
    	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h3>Eliminar inquilino</h3>
		            </div>
		            <div class="modal-body">
		                ¿Desea eliminar el registro seleccionado? Esta acción es irreversible.
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		                <a class="btn btn-danger btn-ok">Eliminar</a>
		            </div>
		        </div>
		    </div>
		</div>
    
    
    	<div class="alert alert-info">     		 	
   		 	<div>
				<form method="GET" class="form-inline" action="<c:url value="/inquilino"/>">		
					<div class="form-group">
						<label for="dni">DNI</label>
						<input type="text" name="dni" class="form-control" value="${dni}"/>
					</div>
					<button type="submit" class="btn btn-success">Buscar <span class="glyphicon glyphicon-search"></span></button>	
				</form>
   		 	</div> 	<br>
    	  	
   		 	
   		 	<div>
				<form method="GET" class="form-inline" action="<c:url value="/inquilino"/>">					
					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input type="text" name="nombre" class="form-control" value="${nombre}"/>
					</div>
					
					<div class="form-group">
						<label for="aPaterno">Apellido Paterno</label>
						<input type="text" name="aPaterno" class="form-control" value="${aPaterno}"/>
					</div>
					
					<div class="form-group">
						<label for="aMaterno">Apellido Materno</label>
						<input type="text" name="aMaterno" class="form-control" value="${aMaterno}"/>
					</div>
					<button type="submit" class="btn btn-success">Buscar <span class="glyphicon glyphicon-search"></span></button>	
				</form>
			</div>
		</div>
    	
    	<div>
    		<a href="<c:url value="/inquilino/agregar"/>" class="btn btn-success">Agregar  <span class="glyphicon glyphicon-plus"></span></a>
    	</div><br>
    	<div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Ap. Paterno</th>
						<th>Ap. Materno</th>
						<th>DNI</th>
						<th>Acciones</th>
					</tr>
				</thead>
				
				<tbody>			
					<c:forEach var = "inquilino" items="${inquilinos}">
						<tr>
							<td><c:out value = "${inquilino.nombre}"/></td>
							<td><c:out value = "${inquilino.aPaterno}"/></td>
							<td><c:out value = "${inquilino.aMaterno}"/></td>
							<td><c:out value = "${inquilino.dni}"/></td>
							<td>
								<div class="col-sm-6">
									<a href="<c:url value="/inquilino/editar?id=${inquilino.id}"/>"><span class="glyphicon glyphicon-pencil"></span></a>
								</div>
	    						    			
								<div class="col-sm-6">		
									<a href="#" data-href="<c:url value="/inquilino/eliminar?id=${inquilino.id}"/>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></a>	
	    							<!--  a href="<c:url value="/inquilino/eliminar?id=${inquilino.id}"/>"><span class="glyphicon glyphicon-trash"></span></a-->
	    						</div>
							</td>
						</tr>
			      	</c:forEach>
				</tbody>
			</table>			
				
			<c:if test = "${inquilinos.size()==0}">
			    <h3 class="text-center">No se encontraron registros</h3>
			</c:if>
    	</div>
   		
   		<script>
	   		$('#confirm-delete').on('show.bs.modal', function(e) {
	   		    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	   		});
   		</script>
   		
    	<style>
    		.table td,th {
			   text-align: center;   
			}
    	</style>
    </jsp:body>
</t:layout>