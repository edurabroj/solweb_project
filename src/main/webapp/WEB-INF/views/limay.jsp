<!DOCTYPE html>
<html lang="es">
<head>
  <title>Las Fuentes</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Indie+Flower" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Diplomata+SC|Indie+Flower" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Diplomata+SC|Fredericka+the+Great|Indie+Flower" rel="stylesheet">
</head>

<style type="text/css">
  
  {
    margin: 0;
    padding: 0;
    font-family: helvetica;

  }
  section#banner{
    width: 100%;
    background-image: url('http://tooeasy.es/wp-content/uploads/2014/05/fondo-tooeasy2.jpg');
    background-attachment: contain;/*toda la pantalla*/
    background-repeat: no-repeat;

  }

  header{
    position: fixed;
    width: 100%;
    height: 70px;
    background-color: transparent;
    z-index:99999999;

  }
  nav{
    display: table;
    margin: auto;
    margin-top: 15px;
    height: 40px;
    background-color: transparent;
  }

  ul{
    display: block;
    float:  left;
    list-style: none;

  }

  a#logo{
      float: left;     
      display: block;
      margin-left: 30px;
      margin-right: 30px;
      margin-top: 150px; 
  }

  a#logo{
    font-family: 'Fredericka the Great', cursive;
    font-size: 50px;
    text-decoration: none;
    cursor: none;
    color: #fff;

  }

  ul li{
    float:  left;
    margin:10px;
    padding: 2px;
    color: #fff;
  }

  li a{
    font-family: 'Indie Flower', cursive;
    font-size: 20px;
  }

  ul a>li{
    text-decoration: none

  }

  ul a>li:hover{
    text-decoration: underline;
  }

  footer{
    text-align: center;
    background-color: #000;
    font: italic normal 400 16px/22px Arial, Verdana, Sans-serif;

  }

  footer>span{
    color: #fff;
    font-size: 15px;
    margin-bottom :0px

  }


  h4{
    text-align: center;
  }

  ..dropdown-menu{

  }

</style>
<body>

  <header id="header">
      <nav> 
        <ul>  
          <li class="content-link"><a href="#banner" > Inicio </a> </li>

          <li id="dropdown" class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                  Cuenta <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                  <li><a href="#">Inquilinos</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Propiedades</a></li>
                  <li><a href="#">Alquiler</a></li>
                  <li class="divider"></li>
              </ul>
          </li>  
        </ul>
              <a style="text-align: center;" id="logo" href="#banner">SISTEMA DE GESTION<br>Y<br>CONTROL DE ALQUILERES</a>
         <ul>  
          <li class="content-link"><a href="#nosotros" > Nosotros </a></li>
          <li class="content-link"><a href="/" > Login </a></li>
        </ul>


      </nav>
    
  </header>
  
  <section id="banner" style="height: 640px">
    
  </section>  

  <footer class="size">
      <span> Copyright@2017. Todos los derechos reservados. <a href="#">ByteToByte</a></span>
  </footer>


  <script>
      $(document).ready(function(){

          $("#dropdown").hover(
            function(){
              $(this).addClass('show');
            },
            function(){
              $(this).removeClass('show');
            }
          );

          $('.content-link').click(function(){
              var $href = $(this).attr('href');
              var $anchor = $($href).offset();
              window.scrollTo($anchor.left,$anchor.top);
              return false;
          });
 

          var flag = false;
          var scroll;

          $(window).scroll(function(){
            scroll = $(window).scrollTop();

            if(scroll>200){
              if (!flag) {
                $("#header").css({
                  "background-color": "rgb(247, 247, 247)",
                });
                $("#logo").css({
                  "margin-top":"-5px",
                  "font-size":"13px",
                  "color":"#014c8c",
                  
                });
                flag = true;
              }
            }else{
              if (flag){
                  $("#header").css({
                    "background-color": "transparent",
                  });
                  $("#logo").css({
                    "margin-top":"150px",
                    "font-size":"33px",
                    "color":"#fff",
                  });
                  flag = false;
              }
            }
          })
      });
  </script>
</body>
</html>