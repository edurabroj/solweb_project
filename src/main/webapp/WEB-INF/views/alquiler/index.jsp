<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>	
    <jsp:body>   
     	<h2>Alquileres</h2>
    	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h3>Eliminar alquiler</h3>
		            </div>
		            <div class="modal-body">
		                ¿Desea eliminar el registro seleccionado? Esta acción es irreversible.
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		                <a class="btn btn-danger btn-ok">Eliminar</a>
		            </div>
		        </div>
		    </div>
		</div>
    
    
    	<!-- div class="alert alert-info">
    		<div>
				<form method="GET" class="form-inline" action="<c:url value="/propiedad"/>">					
					<div class="form-group">
						<label for="nombre">|d</label>
						<input type="text" name="nombre" class="form-control" value="${inquilinoId}"/>
					</div>
					
					<div class="form-group">
						<label for="direccion">PropiedadId</label>
						<input type="text" name="direccion" class="form-control" value="${propiedadId}"/>
					</div>

					<button type="submit" class="btn btn-success">Buscar <span class="glyphicon glyphicon-search"></span></button>	
				</form>
			</div>
		</div-->
    	
    	<div>
    		<a href="<c:url value="/alquiler/agregar"/>" class="btn btn-success">Agregar  <span class="glyphicon glyphicon-plus"></span></a>
    	</div><br>
    	<div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Inquilino</th>
						<th>Propiedad</th>
						<th>Fecha Inicio</th>
						<th>Mensualidad</th>
						<th>Duración</th>
						<th>Acciones</th>
					</tr>
				</thead>				
				<tbody>			
					<c:forEach var = "item" items="${lista}">
						<tr>
							<td><c:out value = "${item.inquilino.nombre} ${item.inquilino.aPaterno} ${item.inquilino.aMaterno}"/></td>
							<td><c:out value = "${item.propiedad.nombre}"/></td>
							<td><c:out value = "${item.fechaInicio}"/></td>
							<td><c:out value = "${item.mensualidad}"/></td>
							<td><c:out value = "${item.duracionMeses}"/></td>
							<td>
								<div class="col-sm-6">
									<a href="<c:url value="/alquiler/editar?id=${item.id}"/>"><span class="glyphicon glyphicon-pencil"></span></a>
								</div>
	    						    			
								<div class="col-sm-6">		
									<a href="#" data-href="<c:url value="/alquiler/eliminar?id=${item.id}"/>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></a>
	    						</div>
							</td>
						</tr>
			      	</c:forEach>
				</tbody>
			</table>			
				
			<c:if test = "${lista.size()==0}">
			    <h3 class="text-center">No se encontraron registros</h3>
			</c:if>
    	</div>
   		
   		<script>
	   		$('#confirm-delete').on('show.bs.modal', function(e) {
	   		    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	   		});
   		</script>
   		
    	<style>
    		.table td,th {
			   text-align: center;   
			}
    	</style>
    </jsp:body>
</t:layout>