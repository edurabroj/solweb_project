<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:body>    
     	<h2>Alquiler</h2>
		<form:form method="post" commandName="objeto">			
			<div>
				<form:label path="inquilino.id">Inquilino </form:label>			
				
				<form:select path="inquilino.id" cssClass="form-control">
					<form:options items="${inquilinos}" itemValue="id" itemLabel="nombre" cssClass="form-control"/>
				</form:select>
				
				<form:errors path="inquilino.id"></form:errors>				
			</div>			
			<div>
				<form:label path="propiedad.id">Propiedad</form:label>	
				
				<form:select path="propiedad.id" cssClass="form-control">
					<form:options items="${propiedades}" itemValue="id" itemLabel="nombre" cssClass="form-control"/>
				</form:select>
				
				<form:errors path="propiedad.id"></form:errors>
			</div> 		
			<div>
				<form:label path="fechaInicio">Fecha de Inicio</form:label>
				<fmt:formatDate value="${objeto.fechaInicio}" var="dateString" pattern="MM/dd/yyyy" />
				<form:input path="fechaInicio" value="${dateString}" cssClass="form-control datepicker"/>
				<form:errors path="fechaInicio"></form:errors>
			</div> 		
			<div>
				<form:label path="mensualidad">Mensualidad</form:label>
				<form:input path="mensualidad" cssClass="form-control"/>
				<form:errors path="mensualidad"></form:errors>
			</div> 		
			<div>
				<form:label path="duracionMeses">Duracion en meses</form:label>
				<form:input path="duracionMeses" cssClass="form-control"/>
				<form:errors path="duracionMeses"></form:errors>
			</div> <br>	
			<div>
				<input type="submit" value="Guardar" class="btn btn-success"/>
			</div>		
		</form:form>
		
		<script>
			$('.datepicker').datepicker({
			    format: 'mm/dd/yyyy'
			});
		</script>
    </jsp:body>
</t:layout>