<%@tag description="Página Layout" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Sistema de Alquileres</title>	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"/></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css"/>
</head>
<body>
	<div class="container">		
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="<c:url value="/"/>">Sistema de Alquileres</a>
		    </div>
		    <ul class="nav navbar-nav">		    
    			<li><a href="<c:url value="/alquiler"/>">Alquileres </a></li>
    			<li><a href="<c:url value="/inquilino"/>">Inquilinos </a></li>
    			<li><a href="<c:url value="/propiedad"/>">Propiedades </a></li>
		    </ul>
		  </div>
		</nav>
		
		<div id="pageheader">
			<h2><jsp:invoke fragment="header"/></h2>		
		</div>
		
		<div id="body">
		  <jsp:doBody/>
		</div>
		
		<div id="pagefooter" style="text-align:center">
			<hr>
			<style>
			.footer-bottom {
					background: #E3E3E3;
					border-top: 1px solid #DDDDDD;
					padding-top: 10px;
					padding-bottom: 10px;
				}
			</style>
			
			<footer>
				<div class="footer-bottom">
			        <div class="container">
		    			<p id="copyright">Copyright 2017, Byte2Byte en SolWeb</p>
			        </div>
			    </div>
		    </footer>			
		  <jsp:invoke fragment="footer"/>
		</div>
	</div>
</body>
</html>