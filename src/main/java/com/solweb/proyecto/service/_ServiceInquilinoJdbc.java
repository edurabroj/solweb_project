package com.solweb.proyecto.service;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.solweb.proyecto.entities.Inquilino;

public class _ServiceInquilinoJdbc{
	private JdbcTemplate db;
	
	public _ServiceInquilinoJdbc(DriverManagerDataSource dataSource) {
		db = new JdbcTemplate(dataSource);
	}

	public List<Map<String, Object>> getList(String searchCriteria) {
		if (searchCriteria==null || searchCriteria.isEmpty()){
			return db.queryForList("select * from inquilino");
		}else {
			return db.queryForList("select * from inquilino where nombre like ?", '%' + searchCriteria + '%');
		}
	}
	
	public List<Map<String, Object>> getList() {
		return db.queryForList("select * from inquilino");
	}

	public Inquilino getById(int id) {
		Map<String, Object> object = db.queryForMap("select * from inquilino where id=?", id);
		return new Inquilino( Long.parseLong(object.get("id").toString()),
							  object.get("nombre").toString(),
							  object.get("apaterno").toString(),
							  object.get("aMaterno").toString(),
							  object.get("dni").toString());
	}

	public void add(Inquilino object) {
		db.update("insert into inquilino(nombre,aPaterno,aMaterno,dni) values (?,?,?,?)", object.getNombre(), object.getaPaterno(), object.getaMaterno(), object.getDni());		
	}

	public void update(Inquilino object) {
		db.update("update inquilino set nombre=?,aPaterno=?,aMaterno=?,dni=? where id=?", object.getNombre(), object.getaPaterno(), object.getaMaterno(), object.getDni(), object.getId());	
	}

	public void delete(int id) {
		db.update("delete from inquilino where id=?", id);
	}

}
