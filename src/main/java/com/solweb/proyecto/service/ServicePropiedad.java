package com.solweb.proyecto.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solweb.proyecto.data.IDao;
import com.solweb.proyecto.entities.Propiedad;

@Service
public class ServicePropiedad {
	@Autowired
	private IDao dao;
	
	public void persist(Propiedad entity) {
		dao.persist(entity);
	}

	public List<Propiedad> getAll(String... searchCriterias){
		String nombre = searchCriterias[0];
		String direccion = searchCriterias[1];
		
		List<Propiedad> list = dao.getAll(Propiedad.class);
		
		List<Propiedad> filtro;		
		
		if(nombre!=null && !nombre.isEmpty()) {
			filtro = new ArrayList<>();
			
			for(Propiedad p: list) {
				if(p.getNombre().toLowerCase().contains(nombre.toLowerCase())) {
					filtro.add(p);
				}
			}
			
			list = filtro;
		}
		if(direccion!=null && !direccion.isEmpty()) {
			filtro = new ArrayList<>();
			
			for(Propiedad p: list) {
				if(p.getDireccion().toLowerCase().contains(direccion.toLowerCase())) {
					filtro.add(p);
				}
			}
			
			list = filtro;
		}
		return list;
	}
	public void delete(Serializable id) {
		dao.delete(Propiedad.class, id);
	}
	public Propiedad load(Serializable id) {
		return dao.load(Propiedad.class, id);
	}
	public Propiedad get(Serializable id) {
		return dao.get(Propiedad.class, id);
	}
	
	public List<Propiedad> getAll() {
		return dao.getAll(Propiedad.class);
	}
}
