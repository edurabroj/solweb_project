package com.solweb.proyecto.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.solweb.proyecto.entities.Inquilino;
import com.solweb.proyecto.service.ServiceInquilino;
import com.solweb.proyecto.validators.InquilinoValidator;

@RequestMapping("inquilino")
@Controller
public class InquilinoController {
	private final String viewsUrl = "inquilino/";
	
	@Autowired
	private InquilinoValidator inquilinoValidator;
	
	@Autowired
	private ServiceInquilino serviceInquilino;
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "index");
		
		String dni = req.getParameter("dni");
		String nombre = req.getParameter("nombre");
		String aPaterno = req.getParameter("aPaterno");
		String aMaterno = req.getParameter("aMaterno");
		mav.addObject("inquilinos", serviceInquilino.getAll(dni,nombre,aPaterno,aMaterno));
		
		mav.addObject("dni", dni);
		mav.addObject("nombre", nombre);
		mav.addObject("aPaterno", aPaterno);
		mav.addObject("aMaterno", aMaterno);
		return mav;
	}	
	
	@RequestMapping(value="/agregar", method=RequestMethod.GET)
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		mav.addObject("inquilino", new Inquilino());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("inquilino") Inquilino inquilino,
				BindingResult result
		)
	{
		inquilinoValidator.validate(inquilino, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("inquilino", inquilino);
			return mav;		
		}else {
			serviceInquilino.persist(inquilino);
			return new ModelAndView("redirect:/inquilino");
		}
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.GET)
	public ModelAndView editar(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		
		Long id = Long.parseLong(request.getParameter("id"));
		mav.addObject("inquilino", serviceInquilino.get(id));
		return mav;
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.POST)
	public ModelAndView editar
	(
		@ModelAttribute ("inquilino") Inquilino inquilino,
		BindingResult result
	)
	{
		inquilinoValidator.validate(inquilino, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("inquilino", inquilino);
			return mav;		
		}else {
			serviceInquilino.persist(inquilino);
			return new ModelAndView("redirect:/inquilino");
		}
	}
	
	@RequestMapping(value="/eliminar", method=RequestMethod.GET)
	public ModelAndView eliminar(HttpServletRequest request)
	{
		Long id = Long.parseLong(request.getParameter("id"));
		serviceInquilino.delete(id);
		return new ModelAndView("redirect:/inquilino");
	}
}
