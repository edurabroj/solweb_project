package com.solweb.proyecto.validators;

import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.solweb.proyecto.entities.Inquilino;
import com.solweb.proyecto.service.ServiceInquilino;

@Component
public class InquilinoValidator implements Validator {
	@Autowired
	private ServiceInquilino serviceInquilino;
	
	String dniPattern = "^[0-9]{8}";
	
	@Override
	public boolean supports(Class<?> type) {
		return Inquilino.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Inquilino inquilino = (Inquilino) o;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "required.nombre","El nombre es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "aPaterno", "required.aPaterno","El ap. paterno es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "aMaterno", "required.aMaterno","El ap. materno es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dni", "required.dni","El DNI es obligatorio");

		if(! Pattern.compile( dniPattern ).matcher( inquilino.getDni() ).matches()){
			errors.rejectValue("dni", "dniLength", "El DNI debe estar compuesto por 8 d�gitos.");
		}
		
		if(inquilino.getDni()!=null && !inquilino.getDni().isEmpty())
			if(serviceInquilino.dniYaRegistrado(inquilino)) {
				errors.rejectValue("dni", "dniNotUnique", "El DNI ya est� registrado en la base de datos.");
			}
	}

}
