package com.solweb.proyecto.data;

import java.io.Serializable;
import java.util.List;

public interface IDao {
	public void persist(Object entity);	 
	public <T> List<T> getAll(Class<T> entityClass); 
	public <T> void delete(Class<T> entityClass, Serializable id);
	public <T> T load(Class<T> entityClass, Serializable id); 
	public <T> T get(Class<T> entityClass, Serializable id); 
	public <T> List<T> find(String hql);
}