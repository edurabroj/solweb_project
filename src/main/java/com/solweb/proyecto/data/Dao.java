package com.solweb.proyecto.data;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class Dao extends HibernateDaoSupport implements IDao {	
	@Autowired
	public Dao(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
 
	@Transactional
	public void persist(Object entity) {
		getHibernateTemplate().saveOrUpdate(entity);
	}
	
	@Transactional
	public <T> void delete(Class<T> entityClass, Serializable id) {
		T entity = load(entityClass, id);
		getHibernateTemplate().delete(entity);
	}
 
	@Transactional(readOnly = true)
	public <T> List<T> getAll(Class<T> entityClass) {
		final List<T> entities = getHibernateTemplate().loadAll(entityClass);
		return entities;
	}
 
	@Transactional(readOnly = true)
	public <T> T load(Class<T> entityClass, Serializable id) {
		final T entity = (T)getHibernateTemplate().load(entityClass, id);
		return entity;
	}
	
	@Transactional(readOnly = true)
	public <T> T get(Class<T> entityClass, Serializable id) {
		final T entity = (T)getHibernateTemplate().get(entityClass, id);
		return entity;
	}
 
	@Transactional(readOnly = true)
	public <T> List<T> find(String hql) {
		final List<T> entities = getHibernateTemplate().find(hql);
		return entities;
	}
}
